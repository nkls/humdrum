# Humdrum.js

[![experimental](http://badges.github.io/stability-badges/dist/experimental.svg)](http://github.com/badges/stability-badges) (don't use it...)

Communicative wrapper around immutable.js data structures. For truly collaborative ReactJS interfaces.

## Things to solve

- Cache the internal structure to timestamps so that we don't need to apply all patches every time (a free strength that comes with using immutable structures for this).
- Add support for creating streams for specified paths in the structure.
- Add better example with websockets.
- Maybe support collaborative text editing (probably needs a new Immutable.js type based on Seq)?
- ... certainly other things.

## Shoutout

I had the idea, but the implementation is basically these fine projects put together:
- [Immutable.js](https://github.com/facebook/immutable-js)
- [Scuttlebutt](https://github.com/dominictarr/scuttlebutt)
- [Immutable Patch](https://github.com/intelie/immutable-js-patch)
- [Immutable Diff](https://github.com/intelie/immutable-js-diff)

License: ISC
