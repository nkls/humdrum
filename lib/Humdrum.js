var immutable      = require('immutable');
var EventEmitter   = require('events').EventEmitter;
var inherits       = require('inherits');
var diffImmutable  = require('immutablediff');
var patchImmutable = require('immutablepatch');
var Cursor         = require('immutable/contrib/cursor');
var UpdatesStore   = require('./UpdatesStore');


inherits(Humdrum, EventEmitter);

function Humdrum (id, initial) {
  if (!(this instanceof Humdrum)) return new Humdrum(id, initial);

  this._updatesStore = new UpdatesStore(id);
  this._updatesStore.on('_update', invalidateStruct.bind(this));

  this._initial = immutable.fromJS(initial || {});
  this._structure = null;
}

var proto = Humdrum.prototype;


proto.get = function () {
  return this._structure || (this._structure = applyUpdates(this._initial, this._updatesStore.updates));
};

/**
 * Creates a cursor from the internal structure. Updates through this cursor and
 * subcursors of this cursor will trigger internal change events that can be used for
 * reconciliation.
 *
 * @param  {Array} [path]       Path for this cursor.
 *
 * @return {immutable.Cursor}   Return a new Cursor for the given path.
 */
proto.cursor = function (path) {
  path = path || [];

  var localUpdate = this._updatesStore.localUpdate.bind(this._updatesStore)
  var self = this;

  return Cursor.from(this.get(), path, function (newStructure, oldStructure) {
    var patches = diffImmutable(oldStructure, newStructure).toJS();
    
    patches.forEach(function (patch) {
      localUpdate(patch);
    });
  });
};

proto.createStream = function () {
  return this._updatesStore.createStream.apply(this._updatesStore, arguments);
};

/**
 * Convert the internal structure to regular js. See immutable.toJS.
 *
 * @return {*}    Returns regular javascript.
 */
proto.toJS = proto.toJSON = function () {
  return this.get().toJS();
};

function invalidateStruct () {
  this._structure = null;
  this.emit('update');
}

function applyUpdates (structure, updates) {
  if (updates.length === 0) return structure;

  var patches = updates.map(getPatchFromUpdate);
  return patchImmutable(structure, immutable.fromJS(patches));
}

function getPatchFromUpdate (update) {
  return update[0];
}

module.exports = Humdrum;

