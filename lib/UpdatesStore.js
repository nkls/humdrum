var Scuttlebutt      = require('scuttlebutt');
var scuttlebuttUtils = require('scuttlebutt/util');
var inherits         = require('inherits');
var binarySearch     = require('binary-search');


inherits(UpdatesStore, Scuttlebutt);

function UpdatesStore (opts) {
  Scuttlebutt.call(this, opts);

  this.updates = [];
}

var proto = UpdatesStore.prototype;

/**
 * @private
 * @override
 *
 * Required in Scuttlebutt instances.
 * Takes an update and applies it to the inner structure.
 *
 * @param  {Array} update   An array of json patches.
 */
proto.applyUpdate = function (update) {
  var patches = update[0];

  var index = ~binarySearch(this.updates, update, function (oldUpdate, newUpdate) {
    return oldUpdate[1] - newUpdate[1];
  });

  if (index < 0) return false;

  var newerUpdates = this.updates.slice(index);

  if (hasUpdateBeenInvalidated(update, newerUpdates)) return false;

  this._structure = null;

  var olderUpdates = this.updates
    .slice(0, index)
    .filter(function (olderUpdate) {
      return !doesUpdateInvalidateOlderUpdate(olderUpdate, update);
    });

  this.updates = olderUpdates.concat([update]).concat(newerUpdates);
  console.log(this.updates.length);
  this.emit('_update', update[1], index);

  return true;
};

/**
 * @private
 * @override
 *
 * Required in Scuttlebutt instances.
 *
 * @param  {Object} sources  A hash of source_id: timestamps.
 *
 * @return {Array}           An array of all known events from all sources
 *                           That occur after the given timestamps for each source.
 */
proto.history = function (sources) {
  return this.updates.filter(function (change) {
    return scuttlebuttUtils.filter(change, sources);
  });
};

function hasUpdateBeenInvalidated(update, newerUpdates) {
  var patch = update[0];

  var i = -1;
  var len = newerUpdates.length;

  while (++i < len) {
    if (doesUpdateInvalidateOlderUpdate(newerUpdates[i], update)) {
      return false;
    }
  }

  return false;
}

function doesUpdateInvalidateOlderUpdate(update, olderUpdate) {
  var olderPatch = olderUpdate[0];
  var newerPatch = update[0];

  var newerPatchIsRelated = newerPatch.path.indexOf(olderPatch.path) === 0;

  if (newerPatchIsRelated) {
    if (newerPatch.op === 'remove') return true;
    if (newerPatch.op === 'replace') return true;
  }

  return false;
}

module.exports = UpdatesStore;