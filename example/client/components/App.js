var react = require('react');
var Table = require('./Table');

var submitButtonStyles = require('./styles/submitButtonStyles');

var el = react.createElement;

module.exports = react.createClass({
  displayName: 'App',

  handleAddItemClick: function (event) {
    event.preventDefault();

    this.props.cursor.cursor(['items']).set(generateUUID(), {a: 0, b: 0, c: 0, d: 0, e: 0, f: 0});
  },

  render: function () {
    return el('div', null,
      el(Table, {cursor: this.props.cursor.cursor(['items'])}),
      el('a', {
        href: '#',
        onClick: this.handleAddItemClick,
        style: submitButtonStyles.toJS(),
        title: 'Append new row to the end'
      }, '+ Add new row')
    );
  }
});

function generateUUID() {
  var d = new Date().getTime();

  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random()*16)%16 | 0;
    d = Math.floor(d/16);

    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });

  return uuid;
};