var react = require('react');

var TableCell = require('./TableCell');
var warningButtonStyles = require('./styles/warningButtonStyles');

var el = react.createElement;

module.exports = react.createClass({
  displayName: 'TableRow',

  handleRemove: function () {
    this.props.onDelete(this.props.index);
  },
  
  renderCells: function () {
    var item = this.props.item;

    return item.keySeq().map(function (key) {
      return el(TableCell, {value: item.cursor(key)});
    }).toJS().concat(this.renderRemoveButton());
  },

  renderRemoveButton: function () {
    return el('td', null,
      el('a', {
        onClick: this.handleRemove,
        href: '#',
        style: warningButtonStyles.toJS(),
        title: 'Delete row'
      }, 'x')
    );
  },

  render: function () {
    return el('tr', null, this.renderCells());
  }
});