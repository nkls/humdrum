var react = require('react');

var TableRow = require('./TableRow');

var el = react.createElement;

module.exports = react.createClass({
  displayName: 'Table',

  handleRowDelete: function (index) {
    this.props.cursor.delete(index);
  },

  renderRows: function () {
    var handleRowDelete = this.handleRowDelete;

    return this.props.cursor.map(function (item, index) {
      return el(TableRow, {item: item, index: index, onDelete: handleRowDelete});
    }).toJS();
  },

  render: function () {
    return el('table', null,
      el('tbody', null, this.renderRows())
    );
  }
});