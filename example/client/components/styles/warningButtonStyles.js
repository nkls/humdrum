var buttonStyles = require('./buttonStyles');

module.exports = buttonStyles.merge({
  borderColor: 'red',
  background: 'pink',
  color: 'darkred'
});