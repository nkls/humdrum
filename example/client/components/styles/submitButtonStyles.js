var buttonStyles = require('./buttonStyles');

module.exports = buttonStyles.merge({
  borderColor: 'green',
  background: 'lightgreen',
  color: 'darkgreen'
});