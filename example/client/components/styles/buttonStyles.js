var immutable = require('immutable');

module.exports = immutable.Map({
  border: '1px solid black',
  borderRadius: 3,
  background: 'grey',
  display: 'inline-block',
  color: 'black',
  padding: '4px 8px',
  textDecoration: 'none',
  fontSize: 10,
  textTransform: 'uppercase',
  fontFamily: 'sans-serif',
  fontWeight: 'bold'
});