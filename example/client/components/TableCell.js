var react = require('react');

var el = react.createElement;

module.exports = react.createClass({
  displayName: 'TableCell',

  handleChange: function (event) {
    this.props.value.update(function () { return event.target.value; });
  },

  render: function () {
    return el('td', null,
        el('input', {onChange: this.handleChange, value: this.props.value.deref()})
    );
  }
});