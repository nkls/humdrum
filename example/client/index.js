var react           = require('react');
var WebsocketStream = require('websocket-stream');
var structure       = require('../lib/structure');
var kv              = require('kv')('kv:');
var App             = require('./components/App');

var socketStream = WebsocketStream('ws://192.168.1.71:9988');

var structureStream = structure.createStream();

// Persist to local storage
// kv.get('test').pipe(structure.createStream({writable: true, readable: false}));
// structure.createStream({writable: false, readable: true}).pipe(kv.put('test'));

// Sync with server
structureStream.pipe(socketStream).pipe(structureStream);

var el = react.createElement;

structure.on('update', function () {
  render();
});

render();

function render () {
  react.render(react.createElement(App, {cursor: structure.cursor([])}), document.getElementById('app'));
}
