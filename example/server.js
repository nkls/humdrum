var http            = require('http');
var browserify      = require('browserify-middleware');
var express         = require('express');
var WebSocketServer = require('ws').Server;
var WebSocketStream = require('websocket-stream');
var structure       = require('./lib/structure');

var app = express();

app.use(express.static(__dirname + '/public'));
app.get('/client.js', browserify('./client/index.js'));

var httpServer = http.createServer(app);

var wsServer = new WebSocketServer({server: httpServer});

wsServer.on('connection', function (ws) {
    var socketStream = WebSocketStream(ws);
    var structureStream = structure.createStream();

    structureStream.pipe(socketStream).pipe(structureStream);
});

httpServer.listen(9988);
console.log('Listening on port 9988');