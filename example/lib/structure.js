var Humdrum = require('../../lib/Humdrum');
var immutable = require('immutable');

module.exports = Humdrum('josefine', {
    items: immutable.OrderedMap()
});
